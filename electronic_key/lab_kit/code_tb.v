// Design: code
// Description: Digital code key. Test bench
// Author: Jorge Juan <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Date: 17-01-2021

`timescale 1ns / 1ps

// Test bench

module test();

    reg clk = 1;        // clock
    reg reset = 0;      // reset
    reg [3:0] b;        // input
    wire z;             // output

	// Unit Under Test module instantiation
    code uut (.clk(clk), .reset(reset), .b(b), .z(z));

    initial begin
		// Waveform generation
        $dumpfile("code_tb.vcd");
        $dumpvars(0, test);

        // Inicialización
        #5  reset = 1;
            b = 4'b0000;
        #10 reset = 0;
        #15 ;

        // Señales de entrada
        #20    b = 4'b0001;    // '0' pressed
        #20    b = 4'b0001;
        #20    b = 4'b0001;
        #20    b = 4'b0000;    // '0' released
        #20    b = 4'b0000;
        #20    b = 4'b0100;    // '2' pressed
        #20    b = 4'b0100;
        #20    b = 4'b0100;
        #20    b = 4'b0100;
        #20    b = 4'b0000;    // '2' released
        #20    b = 4'b0000;
        #20    b = 4'b0000;
        #20    b = 4'b0000;
        #20    b = 4'b0010;    // '1' pressed
        #20    b = 4'b0010;
        #20    b = 4'b0010;
        #20    b = 4'b0010;
        #20    b = 4'b0000;    // '1' released (door should open)
        #20    b = 4'b0000;
        #20    b = 4'b0000;
        #20    b = 4'b1000;    // '3' pressed (la cerradura debe cerrar)
        #20    b = 4'b1000;
        #20    b = 4'b1000;
        #20    b = 4'b0000;    // '3' released
        #20    b = 4'b0000;
        #20    b = 4'b0100;    // some incorrect codes
        #20    b = 4'b0100;
        #20    b = 4'b0100;
        #20    b = 4'b0000;
        #20    b = 4'b0010;
        #20    b = 4'b0010;
        #20    b = 4'b0010;
        #20    b = 4'b0000;
        #20    b = 4'b0000;

        #40    $finish;
    end

	// Clock generator (50 MHz)
    always
        #10    clk = ~clk;
endmodule // test

/*
   You can compile and test this desing in Icarus Verilog executing the
   following command in a terminal:

   $ iverilog code.v code_tb.v
   $ vvp a.out

   You can watch the simulation results with the waveform viewer Gtkwave
   executing the following in a terminal:

   $ gtkwave code_tb.vcd &
 */
