// Design: edge_detector
// Description: Sample edge detector.
// Author: Jorge Juan <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Date: 21-12-2011

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

/*
   The edge detector generates single clock cycle pulse when the input
   changes its value. In this example, only positive (rising) edges are
   detected. An edge detector can be used together with a debouncer to
   generate a clean single cycle pulse from a noisy input.
 */

`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////
// Edge detector                                                        //
//////////////////////////////////////////////////////////////////////////

module edge_detector (
    input wire ck,      // clock
    input wire x,       // input
    output reg z = 0    // output
    );

    reg old_x = 0;

    always @(posedge ck) begin
        old_x <= x;
        if (old_x != x && x == 1'b1)
            z <= 1;
        else
            z <= 0;
    end
endmodule // edge_detector
