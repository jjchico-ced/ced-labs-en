/// Design: code
// Description: Digital code key
// Author: Jorge Juan <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Date: 17-01-2021

/*
   This file describes a siple digital code key. The system has 4 inputs (b0,
   b1, b2 and b3) and one output (z). The output activates (1) when the inputs
   are "pressed" (1) in this sequence: b0, b2, b1.

   Key sequence can start at any time. When the output is active (door open)
   any button press will make it close.

   The system has an asynchronous reset that sets the system to an initial
   known state.
*/

`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////
// Digital code key FSM                                                 //
//////////////////////////////////////////////////////////////////////////

module code(
 	input wire clk,		// clock
 	input wire reset,	// reset
	input wire [3:0] b,	// input buttons
 	output reg z		// output
 	);

 	// State encoding (example)
    parameter [2:0]
        D1_PRESS = 3'b000,   // waiting for first digit press (b0)
        D1_REL   = 3'b001,   // waiting for first digit release (b0)
        D2_PRESS = 3'b010,   // waiting for second digit press (b2)
        D2_REL   = 3'b011,   // waiting for second digit release (b2)
        D3_PRESS = 3'b100,   // waiting for third digit press (b1)
        D3_REL   = 3'b101,   // waiting for third digit release (b1)
        OPEN     = 3'b110;   // door open

	// State and next state variables
    reg [2:0] state, next_state;

	// State change process (sequential part)
    always @(posedge clk, posedge reset)
        if (reset)
            state <= D1_PRESS;
        else
            state <= next_state;

	// Next state calculation process (combinational)
    always @* begin
        next_state = 3'bxxx;
        case (state)
        D1_PRESS:                       // waiting for button 0 press
            if (b == 4'b0001)
                next_state = D1_REL;
            else
                next_state = D1_PRESS;
        D1_REL:                         // waiting for button 0 release
            if (b == 4'b0001)
                next_state = D1_REL;
            else if (b == 4'b0000)
                next_state = D2_PRESS;
            else
                next_state = D1_PRESS;
        D2_PRESS:

            /* write your code here */

        D2_REL:




            /* write your code here */




        OPEN:

            /* write your code here */

        endcase
    end

	// Output calculation process (combinational)
    always @* begin

            /* write your code here */

    end
endmodule // code
