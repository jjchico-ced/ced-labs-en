// Design: calculator
// Description: Simple calculator for Digilent's Basys 2 board
// Author: Jorge Juan <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Date: 13-11-2011

//////////////////////////////////////////////////////////////////////////
// Calculator                                                           //
//////////////////////////////////////////////////////////////////////////

/*
   This design describes a simple unsigned arithmetic unit with four simple
   operations based on the 4-bit operation selection input signal 'op':

    op  | result
   -----+-------
   1000 | a + b
   0100 | a - b
   0010 | b - a
   0001 | a * b

   Operands 'a', 'b' are 4 bits (one hexadecimal digit). Result is 8 bits (two
   hexadecimal digits).
 */

module arithmetic_unit (
    input wire [3:0] op,    // decoded operation selector
    input wire [3:0] a,     // first operand
    input wire [3:0] b,     // second operand
    output reg [7:0] z      // result
    );

  /* write your code here */

endmodule // arithmetic_unit
