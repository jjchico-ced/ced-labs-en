// Design: calculator
// Description: Simple calculator for Digilent's Basys 2 board
// Author: Jorge Juan <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Date: 13-11-2011

`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////
// Calculator test bench                                                //
//////////////////////////////////////////////////////////////////////////

// This test bench applies a configurable number of random input patterns to
// the circuit under test.
// Simulation macros and default values:
//   NP: number of test patterns
//   SEED: initial seed for pseudo-random number generation

`ifndef NP
    `define NP 20
`endif
`ifndef SEED
    `define SEED 1
`endif

module test ();

    reg ck;             // system clock
    reg [3:0] a;        // first operand
    reg [3:0] b;        // second operand
    reg [3:0] op;       // decoded operation selector
    wire [0:6] seg;     // 7-segment output
    wire [3:0] an;      // anode output
    wire dp;            // decimal point output

    // Inititalization of pattern generation variables
    integer np = `NP;
    integer seed = `SEED;

    // Circuit under test
    // Calculator with cdbits reduced to easy simulation of the
    // display controller
    calculator #(.cdbits(3)) uut (.ck(ck), .a(a), .b(b), .op(op),
                 .seg(seg), .an(an), .dp(dp));

    initial begin
        // Initial values for the clock and operation selector
        ck = 1'b0;
        op = 4'b0001;

        // Waveform generation
        $dumpfile("test.vcd");
        $dumpvars(0, test);

        // Output printing
        /* Only arithmetic operands and result (uut's internal signal
           'z') are shown. See waveforms to check 7-segment controller
           operation */
        $display("+--*  A          B           Z");
        $monitor("%b  %b (%d)  %b (%d)   %b (%d)",
                    op, a, a, b, b, uut.z, uut.z);
    end

    // Clock signal required by the display controller
    always
        #20 ck = ~ck;

    // 'a' and 'b' are assigned new random values every 1000 ns. Operation
    // code is changed every 4 operations. Simulation finishes after `NP
    // patterns has been processed.
    always begin
        #1000
        a = $random(seed);
        b = $random(seed);
        if ((np % 4) == 0)
            op = {op[0], op[3:1]};
        np = np - 1;
        if (np == 0)
            $finish;
    end
endmodule
