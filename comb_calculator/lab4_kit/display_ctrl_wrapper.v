// Design: display_ctrl
// Description: Digilent Basys 2 7-segment display controller.
// Author: Jorge Juan <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Date: 13-11-2011

/////////////////////////////////////////////////////////////////////////////
// Display controller wrapper                                              //
// This is just a module declaration to use together with display_ctrl.ngc //
/////////////////////////////////////////////////////////////////////////////

module display_ctrl #(
    parameter cdbits = 18   // clock divider bits
    )(
    input wire ck,          // system clock
    input wire [3:0] x3,    // display digits
    input wire [3:0] x2,
    input wire [3:0] x1,
    input wire [3:0] x0,
    input wire [3:0] dp_in, // decimal point vector
    output reg [0:6] seg,   // 7-segment output
    output reg [3:0] an,    // anode output
    output reg dp           // decimal point output
    );

endmodule // display ctrl
