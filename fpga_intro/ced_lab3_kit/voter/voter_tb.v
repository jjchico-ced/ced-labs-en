// Design:      voter
// Description: Voter circuit. Behavioral description. Test bench.
// Author:      Jorge Juan <jjchico@gmail.com>
// Date:        10-11-2011, 23-11-2020
// Copyright Universidad de Sevilla, Spain

// Time scale and resolution for simulation
`timescale 1ns / 1ps

//
// Test bench (TB)
//
module test();

    // Internal signals
    reg a, b, c;
    wire f;

    // UUT instantiation
    voter uut (.z(f), .a(a), .b(b), .c(c));

    // Signal initialization and simulation control
    initial begin
        // Input signal initialization
        a = 0;
        b = 0;
        c = 0;

        // Waveform generation
        $dumpfile("voter_tb.vcd");  // Waveform file
        $dumpvars(0, test);         // signals to plot (everything)

        // Simulation finishes at t=100
        #100 $finish;
    end

    // Input signal generation
    always			// 'a' is complemented every 5ns making a
        #5 a = ~a;	// square-shaped waveform of 10ns period.
    always
        #10 b = ~b;	// 'b' is 20ns period.
    always
        #20 c = ~c;	// 'c' is 40ns period.

endmodule // test
