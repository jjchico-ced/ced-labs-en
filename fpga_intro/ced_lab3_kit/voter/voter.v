// Design:      voter
// Description: Voter circuit. Behavioral description.
// Author:      Jorge Juan-Chico <jjchico@gmail.com>
// Date:        10-11-2011, 23-11-2020
// Copyright Universidad de Sevilla, Spain

// Time scale and resolution for simulation
`timescale 1ns / 1ps

module voter(

    output reg z,   // output
    input wire a,   // inputs
    input wire b,
    input wire c
    );

    /*** describe your voter circuit here ***/

endmodule // voter
