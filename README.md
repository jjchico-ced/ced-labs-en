Digital Electronic Circuits Labs
================================

Labs for course "Digital Electronic Circuits" of the ETSI Informática
de Sevilla.

This is a personal workspace of the author and does not have to be the same
contents used in the oficial courses.

Lab list
--------

* [Intro](intro/): Introduction to the digital laboratory.

* [Digital circuit design with SSI devices](ssi_voter/): Introduction to digital design using
  SSI devices. Voter circuit.

* [Simple alarm with SSI devices](ssi_alarm/): Simple alarm with...

* [Introduction to FPGA design](fpga_intro/): Introduction to digital circuit
  design using Verilog and FPGA devices. Voter circuit and 7-segment converter.

* [Combination calculator](comb_calculator/): Simple combinational calculator
  design using Verilog. Built around a simple arithmetic circuit.

* [Electronic Key](electronic_key/): Design of key-controlled electronic lock.
  Finite State Machine (FSM) design example.

* [Dice emulator](dice/): Implement an electronic dice emulator using
  counters.

Author
------

Jorge Juan-Chico <jjchico@dte.us.es>

Licence
-------

CC BY-SA 4.0 [Creative Commons Atribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
