// Design: sevenseg
// File: sevenseg_tb.v
// Description: Binary to seven-segment code converter. Test bench.
// 2019-02-13 Jorge Juan-Chico <jjchico@dte.us.es>

`timescale 1ns / 1ps

// Base time used in the simulation
`define BTIME 10

module test();

    // Input signals
    reg [3:0] d;

    // Output signals
    wire [1:7] seg;

    // Unit Under Test
    sevenseg uut (.d(d), .seg(seg));

    // Input initialization and simulation control
    initial begin
        d = 0;

        // Generate waveforms (optinal)
        $dumpfile("sevenseg_tb.vcd");
        $dumpvars(0,test);

        // Signal printing
        $display("d\tseg");
        $monitor("%d\t%b", d, seg);

        // Simulation ends after converting 16 numbers
        #(16*`BTIME) $finish;
    end

    // Input data generation
    always
        #(`BTIME) d = d + 1;

endmodule // test

/*
   You may compile and simulate this design using Icarus Verilog by executing
   the following commands in a terminal:

   $ iverilog sevenseg.v sevenseg_tb.v
   $ vvp a.out

   You can display the simulated waveforms with Gtkwave by executing the
   following command in a terminal:

   $ gtkwave sevenseg_tb.vcd &
 */
