// Design: dice
// File: dice_tb.v
// Description: Electronic dice. Dice test bench.
// 2022-12-13 Jorge Juan-Chico <jjchico@dte.us.es>

// Time scale and simulation resolution
`timescale 1ns / 1ps

//
// Test bench
//
module test();

    reg clk, reset, roll;
    wire [3:0] result;

    // Instance of the Unit Under Test
    // Use a small MAX_PRE_COUNT for easier simulation
    dice #(.MAX_PRE_COUNT(5)) uut
         (.clk(clk), .reset(reset), .roll(roll), .result(result));

    // Clock generation. Clock period is 20ns.
    always
        #10 clk = ~clk;

    // Simulation initialization and control
    initial begin
        // Initial values of input signals
        clk = 0;
        reset = 0;
        roll = 0;

        // Waveform generation
        $dumpfile("dice_tb.vcd");       // Output data file
        $dumpvars(0, test);             // Save all signals

        // Simulation control
        /* Input signal are changed a little after the active clock edge.
        /* After the initial reset, let the dice roll a couple of times.
         */
        @(posedge clk) #1 reset = 1;    // reset system
        @(posedge clk) #1 reset = 0;
        @(posedge clk) #1 roll = 1;     // roll the dice for 40 clock cycles
        repeat(40) @(posedge clk);
        #1 roll = 0;                    // stop the dice for 10 clock cycles
        repeat(10) @(posedge clk);
        #1 roll = 1;                    // roll the dice again for 50 clock cyc.
        repeat(50) @(posedge clk);
        #1 roll = 0;                    // stop the dice again

        // Wait 10 clock cycles and finish the simulation
        repeat(10) @(posedge clk);
        $finish;

    end

endmodule // test