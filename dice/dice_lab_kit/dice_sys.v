// Design: dice
// File: dice_sys.v
// Description: Electronic dice. System integration.
// 2022-12-13 Jorge Juan-Chico <jjchico@dte.us.es>

// Time scale and simulation resolution
`timescale 1ns / 1ps

module dice_sys #(
    // Pre-scaler maximum count value
    // The frequency of the pre-scaler's enable signal is the main clock
    // frequency divided by this number.
    parameter MAX_PRE_COUNT = 5000000       // 10Hz if system clock is 50MHz
    )(
    input wire clk,             // clock
    input wire reset,           // global reset (active low)
    input wire roll,            // roll the dice (count) if set to 1
    output wire [0:6] seg,      // seven segment output
    output wire [3:0] an,       // anode output
    output wire dp              // decimal point output
    );

    /* declare interconnection signals here, if needed */

    // Dice instance
    dice #(
        .MAX_PRE_COUNT(MAX_PRE_COUNT)
        ) dice (
        .clk(clk),

        /* complete signal connections here */

    );

    // 7-segment code converter instance

    /* instantiates the 7-segment code converter here */

    // Anodes and decimal points constant output

    /* assign the anodes and decimal point output signals here */

endmodule // dice_sys