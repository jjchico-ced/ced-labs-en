// Design: dice
// File: dice.v
// Description: Electronic dice. Pre-scaler and dice counters.
// 2022-12-13 Jorge Juan-Chico <jjchico@dte.us.es>

// Time scale and simulation resolution
`timescale 1ns / 1ps

module dice #(
    // Pre-scaler maximum count value
    // The frequency of the pre-scaler's enable signal is the main clock
    // frequency divided by this number.
    parameter MAX_PRE_COUNT = 5000000       // 10Hz if system clock is 50MHz
    )(
    input wire clk,             // clock
    input wire reset,           // global reset (active high)
    input wire roll,            // roll the dice (count) if set to 1
    output reg [3:0] result     // dice value
    );

    // Pre-scaler

    /* pre-scaler design goes here */

    // Dice counter

    /* dice counter design goes here */

endmodule // dice